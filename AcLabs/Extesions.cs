﻿using Autodesk.AutoCAD.DatabaseServices;

namespace AcLab
{
    public static class Extesions
    {
        public static string GetId(this ObjectEventArgs e)
                                => e.DBObject.ObjectId.ToString();

        public static string GetId(this ObjectErasedEventArgs e)
                                => e.DBObject.ObjectId.ToString();

        public static string GetTypeName(this ObjectEventArgs e)
                                => e.DBObject.GetType().ToString();

        public static string GetTypeName(this ObjectErasedEventArgs e)
                                => e.DBObject.GetType().ToString();
    }
}
