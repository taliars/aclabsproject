﻿using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.Windows;
using TreeNode = System.Windows.Forms.TreeNode;

namespace AcLab
{
    public class Commands
    {
        PaletteSet paletteSet;
        UserControl1 userControl;

        Editor CurEditor => Application.DocumentManager.MdiActiveDocument.Editor;
        Database CurDatabase => CurEditor.Document.Database;

        [CommandMethod("AddAnEnt")]
        public void AddAnEnt()
        {
            PromptKeywordOptions promptKeywordOpts = new PromptKeywordOptions
                ("Which entity do you want to create? [Circle/Block]: ", "Circle Block");

            PromptResult promptResult = CurEditor.GetKeywords(promptKeywordOpts);

            if (promptResult.Status != PromptStatus.OK) return;

            if (promptResult.StringResult.Equals("Circle"))
            {
                PromptPointOptions centerPtOpts = new PromptPointOptions("Pick Center Point: ");
                PromptPointResult centerPtRslt = CurEditor.GetPoint(centerPtOpts);

                if (centerPtRslt.Status == PromptStatus.OK)
                {
                    PromptDistanceOptions radiusOpts = new PromptDistanceOptions("Pick Radius: ")
                    {
                        BasePoint = centerPtRslt.Value,
                        UseBasePoint = true
                    };
                    PromptDoubleResult radiusRslt = CurEditor.GetDistance(radiusOpts);

                    if (radiusRslt.Status == PromptStatus.OK)
                    {
                        using (Transaction tr = CurDatabase.TransactionManager.StartTransaction())
                        {
                            Circle circle = new Circle(centerPtRslt.Value, Vector3d.ZAxis, radiusRslt.Value);

                            BlockTableRecord blockTableRecord = tr.GetObject
                                (CurDatabase.CurrentSpaceId, OpenMode.ForWrite) as BlockTableRecord;

                            blockTableRecord.AppendEntity(circle);

                            tr.AddNewlyCreatedDBObject(circle, true);

                            tr.Commit();
                        }
                    }
                }
            }

            if (promptResult.StringResult.Equals("Block"))
            {
                PromptStringOptions blockOpts = new PromptStringOptions("Enter name of the Block to create: ");
                PromptResult blockRslt = CurEditor.GetString(blockOpts);
                // вставка блока
                using (Transaction tr = CurDatabase.TransactionManager.StartTransaction())
                {
                    BlockTable blockTable = tr.GetObject(CurDatabase.BlockTableId, OpenMode.ForRead) as BlockTable;

                    ObjectId blockTableRecordId;

                    if (!blockTable.Has(blockRslt.StringResult))
                    {
                        blockTable.UpgradeOpen();

                        BlockTableRecord blockTableRecord = new BlockTableRecord
                        {
                            Name = blockRslt.StringResult
                        };

                        blockTableRecordId = blockTable.Add(blockTableRecord);

                        tr.AddNewlyCreatedDBObject(blockTableRecord, true);

                        Circle circle1 = new Circle(new Point3d(0, 0, 0), Vector3d.ZAxis, 10);
                        blockTableRecord.AppendEntity(circle1);
                        tr.AddNewlyCreatedDBObject(circle1, true);

                        Circle circle2 = new Circle(new Point3d(20, 10, 0), Vector3d.ZAxis, 10);
                        blockTableRecord.AppendEntity(circle2);
                        tr.AddNewlyCreatedDBObject(circle2, true);
                    }
                    else
                    {
                        blockTableRecordId = blockTable[blockRslt.StringResult];
                    }

                    PromptPointOptions ptOpts =
                            new PromptPointOptions("Pick insertion point of BlockRef: ");

                    PromptPointResult ptRslt = CurEditor.GetPoint(ptOpts);

                    if (ptRslt.Status != PromptStatus.OK) return;

                    BlockReference blockReference = new BlockReference(ptRslt.Value, blockTableRecordId);

                    BlockTableRecord curSpace = tr.GetObject(CurDatabase.CurrentSpaceId, OpenMode.ForWrite) as BlockTableRecord;

                    curSpace.AppendEntity(blockReference);
                    tr.AddNewlyCreatedDBObject(blockReference, true);

                    tr.Commit();
                }
            }
        }

        [CommandMethod("AddData")]
        public void AddData()
        {
            PromptEntityResult entityResult = CurEditor.GetEntity("Pick an entity to add an Extension Dictionaty to: ");

            if (entityResult.Status == PromptStatus.OK)
            {
                using (Transaction tr = CurDatabase.TransactionManager.StartTransaction())
                {
                    Entity ent = tr.GetObject(entityResult.ObjectId, OpenMode.ForRead) as Entity;

                    if (ent.ExtensionDictionary.IsNull)
                    {
                        ent.UpgradeOpen();
                        ent.CreateExtensionDictionary();
                    }

                    DBDictionary dbDict = tr.GetObject(ent.ExtensionDictionary, OpenMode.ForRead) as DBDictionary;

                    if (dbDict.Contains("MyData"))
                    {
                        ObjectId entryId = dbDict.GetAt("MyData");
                        CurEditor.WriteMessage("\nThis entity already has data...");

                        Xrecord xRecord = tr.GetObject(entryId, OpenMode.ForRead) as Xrecord;

                        foreach (TypedValue value in xRecord.Data)
                        {
                            CurEditor.WriteMessage("\n" + value.TypeCode.ToString() + "." + value.Value.ToString());
                        }
                    }
                    else
                    {
                        dbDict.UpgradeOpen();

                        Xrecord xRecord = new Xrecord();

                        ResultBuffer resultBuffer = new ResultBuffer()
                        {
                            new TypedValue((int)DxfCode.Int16, 1),
                            new TypedValue((int)DxfCode.Text, "MyStockData"),
                            new TypedValue((int)DxfCode.Real, 51.9),
                            new TypedValue((int)DxfCode.Real, 100.0),
                            new TypedValue((int)DxfCode.Real, 320.6)
                        };

                        xRecord.Data.Add(resultBuffer);

                        dbDict.SetAt("MyData", xRecord);

                        tr.AddNewlyCreatedDBObject(xRecord, true);

                        if (paletteSet != null)
                        {
                            foreach (TreeNode node in userControl.treeView1.Nodes)
                            {
                                if (node.Tag.ToString() == ent.ObjectId.ToString())
                                {
                                    TreeNode childNode = node.Nodes.Add("Extension Dictionary");

                                    foreach (TypedValue value in xRecord.Data)
                                    {
                                        childNode.Nodes.Add(value.ToString());
                                    }
                                }
                            }
                        }
                    }

                    tr.Commit();
                }
            }
        }
               
        [CommandMethod("Palette")]
        public void Palette()
        {
            if (paletteSet == null)
            {
                paletteSet = new PaletteSet
                    ("My Palette", new System.Guid("D9076E6A-620F-4110-808E-93440A142902"));

                userControl = new UserControl1();

                paletteSet.Add("Palette1", userControl);
            }
            paletteSet.Visible = true;
        }

        [CommandMethod("AddDBEvents")]
        public void AddDbEvents()
        {
            if (paletteSet == null)
            {
                Editor ed = Application.DocumentManager.MdiActiveDocument.Editor;
                ed.WriteMessage("\nPlease call the 'palette' command first");
                return;
            }

            CurDatabase.ObjectAppended += Callback_ObjectAppended;
            CurDatabase.ObjectErased += Callback_ObjectErased;
            CurDatabase.ObjectReappended += Callback_ObjectAppended;
            CurDatabase.ObjectUnappended += Callback_ObjectUnappended;
        }

        [CommandMethod("AddDataToNod")]
        public void AddDataToNod()
        {
            using (Transaction tr = CurDatabase.TransactionManager.StartTransaction())
            {
                DBDictionary nod = tr.GetObject(CurDatabase.NamedObjectsDictionaryId, OpenMode.ForRead) as DBDictionary;

                if (nod.Contains("MyData"))
                {
                    ObjectId entryId = nod.GetAt("MyData");
                    CurEditor.WriteMessage("\nThis entity already has data...");

                    Xrecord xRecord = tr.GetObject(entryId, OpenMode.ForRead) as Xrecord;

                    foreach (TypedValue value in xRecord.Data)
                    {
                        CurEditor.WriteMessage("\n" + value.TypeCode.ToString() + " . " + value.Value.ToString());
                    }
                }
                else
                {
                    nod.UpgradeOpen();

                    Xrecord newRecord = new Xrecord();

                    ResultBuffer resultBuffer = new ResultBuffer()
                    {
                        new TypedValue((int)DxfCode.Int16, 1),
                        new TypedValue((int)DxfCode.Text, "MyCompanyDefaultSettings"),
                        new TypedValue((int)DxfCode.Real, 51.9),
                        new TypedValue((int)DxfCode.Real, 100.0),
                        new TypedValue((int)DxfCode.Real, 320.6)
                    };
                    newRecord.Data.Add(resultBuffer);

                    nod.SetAt("MyData", newRecord);

                    tr.AddNewlyCreatedDBObject(newRecord, true);
                }
                tr.Commit();
            }
        }

        [CommandMethod("AddPointMonitor")]
        public void AddPointMonitor()
        {
            CurEditor.PointMonitor += PointMonitor;
        }

        [CommandMethod("NewInput")]
        public void NewInput()
        {
            CurEditor.PointMonitor += InputMonitor;
            CurEditor.TurnForcedPickOn();

            PromptPointOptions ptOpts = new PromptPointOptions("Pick A Point: ");
            PromptPointResult ptRslt = CurEditor.GetPoint(ptOpts);

            if (ptRslt.Status == PromptStatus.OK)
            {
                //do something&!&!&
            }
            CurEditor.PointMonitor -= InputMonitor;
        }

        public void PointMonitor(object sender, PointMonitorEventArgs e)
        {
            FullSubentityPath[] fullEntPath = e.Context.GetPickedEntities();

            if (fullEntPath.Length > 0)
            {
                using (Transaction tr = CurDatabase.TransactionManager.StartTransaction())
                {
                    Entity ent = tr.GetObject
                        (fullEntPath[0].GetObjectIds()[0], OpenMode.ForRead) as Entity;

                    e.AppendToolTipText($"The entity is a {ent.GetType().ToString()}");

                    if (paletteSet == null) return;

                    System.Drawing.Font fontRegular = new System.Drawing.Font
                        ("Microsoft Sans Serif", 8, System.Drawing.FontStyle.Regular);

                    System.Drawing.Font fontBold = new System.Drawing.Font
                        ("Microsoft Sans Serif", 10, System.Drawing.FontStyle.Bold);

                    userControl.treeView1.SuspendLayout();

                    foreach (TreeNode node in userControl.treeView1.Nodes)
                    {
                        if (node.Tag.ToString() == ent.ObjectId.ToString())
                        {
                            if (!fontBold.Equals(node.NodeFont))
                            {
                                node.NodeFont = fontBold;
                                node.Text = node.Text;
                            }
                        }
                        else
                        {
                            if (!fontRegular.Equals(node.NodeFont))
                            {
                                node.NodeFont = fontRegular;
                            }
                        }
                    }

                    userControl.treeView1.ResumeLayout();
                    userControl.treeView1.Refresh();
                    userControl.treeView1.Update();

                    tr.Commit();
                }
            }
        }

        public void InputMonitor(object sender, PointMonitorEventArgs e)
        {
            if (e.Context == null) return;

            FullSubentityPath[] fullEntPath = e.Context.GetPickedEntities();
            if (fullEntPath.Length > 0)
            {
                using (Transaction tr = CurDatabase.TransactionManager.StartTransaction())
                {
                    Curve ent = (Curve)tr.GetObject(fullEntPath[0].GetObjectIds()[0], OpenMode.ForRead);

                    if (ent.ExtensionDictionary.IsValid)
                    {
                        DBDictionary extensionDict =
                            tr.GetObject(ent.ExtensionDictionary, OpenMode.ForRead) as DBDictionary;

                        ObjectId entryId = extensionDict.GetAt("MyData");

                        Xrecord myXrecord;

                        myXrecord = (Xrecord)tr.GetObject(entryId, OpenMode.ForRead);

                        foreach (TypedValue typedValue in myXrecord.Data)
                        {
                            if (typedValue.TypeCode == (short)DxfCode.Real)
                            {
                                Point3d pt = ent.GetPointAtDist((double)typedValue.Value);

                                Point2d pixels = e.Context.DrawContext.Viewport.GetNumPixelsInUnitSquare(pt);

                                double xDist = (10 / pixels.X);

                                double yDist = (10 / pixels.Y); 

                                Circle circle = new Circle
                                {
                                    Center = pt,
                                    Radius = 35,
                                    Normal = Vector3d.ZAxis
                                };

                                e.Context.DrawContext.Geometry.Draw(circle);                 
                   
                                DBText text = new DBText();
                                text.SetDatabaseDefaults();
                                text.Position = new Point3d(xDist, yDist, 0);
                                text.TextString = typedValue.Value.ToString();
                                text.Height = yDist;

                                e.Context.DrawContext.Geometry.Draw(text);
                            }
                        } 
                    }
                    tr.Commit();
                }
            }
        }

        private void Callback_ObjectAppended(object sender, ObjectEventArgs e)
        {
            TreeNode treeNode = userControl.treeView1.Nodes.Add(e.DBObject.GetType().ToString());
            treeNode.Tag = e.DBObject.ObjectId.ToString();
        }

        private void Callback_ObjectErased(object sender, ObjectErasedEventArgs e)
        {
            if (e.Erased)
            {
                foreach (TreeNode node in userControl.treeView1.Nodes)
                {
                    if (node.Tag.ToString() == e.DBObject.ObjectId.ToString())
                    {
                        node.Remove();
                        break;
                    }
                }
            }
            else
            {
                TreeNode newNode = userControl.treeView1.Nodes.Add(e.DBObject.GetType().ToString());
                newNode.Tag = e.DBObject.ObjectId.ToString();
            }
        }

        private void Callback_ObjectUnappended(object sender, ObjectEventArgs e)
        {
            foreach (TreeNode node in userControl.treeView1.Nodes)
            {
                if (node.Tag.ToString() == e.DBObject.ObjectId.ToString())
                {
                    node.Remove();
                    break;
                }
            }
        }
    }
}
